package com.appgranula.ingame.prefs;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Belozerov on 01.09.2014.
 */
@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface WatchingPref {
    @DefaultBoolean(true)
    public boolean showAgain();
}

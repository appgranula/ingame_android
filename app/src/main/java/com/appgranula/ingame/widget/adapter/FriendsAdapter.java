package com.appgranula.ingame.widget.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.widget.FriendView;
import com.appgranula.ingame.widget.FriendView_;
import com.appgranula.ingame.widget.GroupView;
import com.appgranula.ingame.widget.GroupView_;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Belozerov on 12.08.2014.
 */
public class FriendsAdapter extends BaseAdapter implements StickyListHeadersAdapter {
    public static final int TYPE_GROUP = 0;
    public static final int TYPE_USER = 1;
    List<WotUser> userList;
    LayoutInflater layoutInflater;
    int ingameTextColor;
    int notIngameTextColor;
    int ingameStatusColor;
    int ingameSendingColor;
    Resources resources;
    int onlineColor;
    Context context;
    private OnCustomButtonClickListener onCustomButtonClickListener;

    public FriendsAdapter(Context context) {
        super();
        this.context = context;
        resources = context.getResources();
        layoutInflater = LayoutInflater.from(context);
        ingameTextColor = resources.getColor(R.color.ingameTextColor);
        notIngameTextColor = resources.getColor(R.color.notIngameTextColor);
        ingameSendingColor = resources.getColor(R.color.ingameSendingColor);
        ingameStatusColor = resources.getColor(R.color.ingameStatusColor);
        onlineColor = resources.getColor(R.color.onlineColor);
    }

    public void setOnCustomButtonClickListener(OnCustomButtonClickListener onCustomButtonClickListener) {
        this.onCustomButtonClickListener = onCustomButtonClickListener;
    }

    public void setUserList(List<WotUser> userList) {
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList == null ? 0 : userList.size();
    }

    @Override
    public WotUser getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return userList.get(i).account_id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        switch (getItemViewType(i)) {
            case TYPE_USER:
                return getUserView(i, view, viewGroup);
            case TYPE_GROUP:
                return getGroupView(i, view, viewGroup);
            default:
                return null;
        }

    }

    private View getUserView(int position, View view, ViewGroup viewGroup) {
        FriendView friendView;
        if (view == null) {
            friendView = FriendView_.build(context);
        } else {
            friendView = (FriendView) view;
        }

        friendView.bind(getItem(position), onCustomButtonClickListener);
        return friendView;
    }

    private View getGroupView(int position, View view, ViewGroup viewGroup) {
        GroupView groupView;
        if (view == null) {
            groupView = GroupView_.build(context);
        } else {
            groupView = (GroupView) view;
        }
        groupView.bind(getItem(position), onCustomButtonClickListener);
        return groupView;
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        if (getHeaderId(i) == 0)
            return layoutInflater.inflate(R.layout.dummy, viewGroup, false);
        return layoutInflater.inflate(R.layout.friend_header, viewGroup, false);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        WotUser wotUser = getItem(position);
        return wotUser.isGroup() ? 0 : 1;
    }

    @Override
    public long getHeaderId(int i) {
        WotUser user = userList.get(i);
        return user.hasIngame() ? TYPE_GROUP : TYPE_USER;
    }

    public interface OnCustomButtonClickListener {
        void onClick(WotUser wotUser);
    }

    private static class ViewHolderFriend {
        TextView nickname;
        View sending;
        View sent;
        View sendingProgress;
        View isOnline;
        View isWatching;
    }

    private static class ViewHolderGroup {
        TextView name;
        View infoButton;
        TextView online;
        View sending;
        View sent;
        View sendingProgress;
    }
}

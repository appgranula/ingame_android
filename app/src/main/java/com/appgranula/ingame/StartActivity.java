package com.appgranula.ingame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

import com.appgranula.ingame.fragments.AuthFragment;
import com.appgranula.ingame.fragments.AuthFragment_;
import com.appgranula.ingame.fragments.ChooseRegionFragment;
import com.appgranula.ingame.fragments.ChooseRegionFragment_;
import com.appgranula.ingame.prefs.UserData_;
import com.parse.ParseAnalytics;
import com.parse.ParseUser;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Created by Belozerov on 13.08.2014.
 */
@EActivity(R.layout.activity_start)
public class StartActivity extends ActionBarActivity {
    @ViewById
    ListView regionList;
    @Pref
    UserData_ userData;
    AuthFragment_ authFragment;
    ChooseRegionFragment_ chooseRegionFragment;


    private boolean isChoose() {
        return chooseRegionFragment != null && chooseRegionFragment.isResumed();
    }

    private boolean isAuth() {
        return authFragment != null && authFragment.isResumed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (isAuth()) {
            authFragment.getWebView().saveState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (isAuth()) {
            authFragment.getWebView().restoreState(savedInstanceState);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ParseAnalytics.trackAppOpened(getIntent());
        if (ParseUser.getCurrentUser() != null) {
            goToMain();
            return;
        }

        if (savedInstanceState == null) {
            chooseRegionFragment = new ChooseRegionFragment_();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, chooseRegionFragment, ChooseRegionFragment.TAG)
                    .commitAllowingStateLoss();
        }
    }

    private void goToMain() {
        startActivity(new Intent(this, MainActivity_.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        if (isAuth()) {
            if (authFragment.getWebView().canGoBack()) {
                authFragment.getWebView().goBack();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public void showWebView() {
        authFragment = new AuthFragment_();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.fragmentContainer, authFragment, AuthFragment.TAG);
        transaction.addToBackStack(AuthFragment.TAG);
        transaction.commitAllowingStateLoss();
    }

    public void showRegionFragment() {
        super.onBackPressed();
    }
}

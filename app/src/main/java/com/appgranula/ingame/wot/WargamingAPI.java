package com.appgranula.ingame.wot;

import android.net.Uri;
import android.text.TextUtils;

import com.appgranula.ingame.prefs.UserData_;
import com.appgranula.ingame.utils.Log_;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Belozerov on 12.08.2014.
 */
@EBean
public class WargamingAPI {
    public static final HashMap<String, String> WARGAMING_APP_ID = new HashMap<String, String>();
    public static final HashMap<String, String> WOT_AUTHORITY = new HashMap<String, String>();
    public static final HashMap<String, String> WOWP_AUTHORITY = new HashMap<String, String>();
    public static final String REDIRECT_URI = "https://api.worldoftanks.ru/wot/blank/";
    public static final String WOT = "wot";
    public static final String WOWP = "wowp";
    private static final String FIELDS = "account_id,clan_id,client_language,last_battle_time,logout_at,nickname,private.friends";
    private static final String FIELDS_WOWP = "account_id,client_language,last_battle_time,logout_at,nickname,private.friends";
    private static final String CLAN_FIENDS = "abbreviation,clan_id,color,name,emblems.large,members";
    @Pref
    UserData_ userData;
    private int loaderCounter = 0;

    static {
        WOT_AUTHORITY.put("ru", "api.worldoftanks.ru");
        WOT_AUTHORITY.put("eu", "api.worldoftanks.eu");
        WOT_AUTHORITY.put("na", "api.worldoftanks.com");
        WOT_AUTHORITY.put("kr", "api.worldoftanks.kr");
        WOT_AUTHORITY.put("asia", "api.worldoftanks.asia");

        WOWP_AUTHORITY.put("ru", "api.worldofwarplanes.ru");
        WOWP_AUTHORITY.put("eu", "api.worldofwarplanes.eu");
        WOWP_AUTHORITY.put("na", "api.worldofwarplanes.com");
        WOWP_AUTHORITY.put("kr", "api.worldofwarplanes.kr");
        WOWP_AUTHORITY.put("asia", "api.worldofwarplanes.asia");

        WARGAMING_APP_ID.put("ru", "0ba576f27f3b0dc95cd56bf18733b1f1");
        WARGAMING_APP_ID.put("asia", "31660921fc05fd4a81c273238d16b9d2");
        WARGAMING_APP_ID.put("eu", "ebf53a8e1d483cd16166e0538f5beff6");
        WARGAMING_APP_ID.put("na", "0391a11c5fa1966d5aed615427834e92");
        WARGAMING_APP_ID.put("kr", "30d648c15db90131fc147c16a377d65e");
    }

    public void prolongateAccessTokenIfNeeded() {
        if (TextUtils.isEmpty(userData.nickname().get())) {
            return;
        }
        String url = getUri(WOT)
                .appendPath("auth")
                .appendPath("prolongate")
                .appendPath("")
                .build().toString();
        JSONObject answer = requestPost(url, getDefaultParams());
        if (isOk(answer)) {
            Log_.i("Successfully prolongate access token");
            try {
                JSONObject answerData = answer.getJSONObject("data");
                userData.getSharedPreferences().edit().putLong(
                        userData.expiresAt().key(), answerData.getLong("expires_at")
                ).putString(
                        userData.accessToken().key(), answerData.getString("access_token")
                ).commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log_.w("Token prolongation failed");
        }
    }

    private List<NameValuePair> getDefaultParams() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", userData.accessToken().get()));
        params.add(new BasicNameValuePair("application_id", getAppId()));
        return params;
    }

    public String getAppId() {
        return WARGAMING_APP_ID.get(userData.region().get());
    }

    @Background
    public void requestClanInfo(WotUser user, String game, APICallback apiCallback) {
        String url = getUri(game)
                .appendPath("clan")
                .appendPath("info")
                .appendPath("")
                .appendQueryParameter("application_id", getAppId())
                .appendQueryParameter("clan_id", Long.toString(user.clan_id))
                .appendQueryParameter("access_token", userData.accessToken().get())
                .appendQueryParameter("fields", CLAN_FIENDS)
                .toString();
        JSONObject answer = request(url);
        if (isOk(answer)) {
            success(apiCallback, answer);
        } else {
            error(apiCallback, answer);
        }
    }

    private void increaseCounter() {
        loaderCounter++;
    }

    private void decreaseCounter() {
        loaderCounter--;
    }

    private JSONObject request(String url) {
        if (url == null)
            return null;
        increaseCounter();
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            return parseResponse(httpResponse);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONObject parseResponse(HttpResponse httpResponse) {
        if (httpResponse != null && httpResponse.getStatusLine().getStatusCode() == 200) {
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                try {
                    InputStream inputStream = httpEntity.getContent();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = bufferedReader.readLine();
                    while (line != null) {
                        stringBuilder.append(line);
                        stringBuilder.append("\n");
                        line = bufferedReader.readLine();
                    }
                    bufferedReader.close();
                    return new JSONObject(stringBuilder.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private JSONObject requestPost(String url, List<NameValuePair> params) {
        if (url == null)
            return null;
        increaseCounter();
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpClient.execute(httpPost);
            return parseResponse(response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Uri.Builder getUri(String game) {
        return new Uri.Builder()
                .scheme("https")
                .authority(WOT.equals(game) ? WargamingAPI.WOT_AUTHORITY.get(userData.region().get()) : WargamingAPI.WOWP_AUTHORITY.get(userData.region().get()))
                .appendPath(game);
    }

    @Background
    public void requestUserInfo(String ids, String game, APICallback apiCallback) {
        Uri.Builder builder = getUri(game)
                .appendPath("account")
                .appendPath("info")
                .appendPath("")
                .appendQueryParameter("application_id", getAppId())
                .appendQueryParameter("account_id", ids)
                .appendQueryParameter("access_token", userData.accessToken().get());
        if(game.equals(WOWP)){
            builder.appendQueryParameter("fields", FIELDS_WOWP);
        } else {
            builder.appendQueryParameter("fields", FIELDS);
        }

        String url = builder.toString();
        JSONObject answer = request(url);
        if (isOk(answer)) {
            success(apiCallback, answer);
        } else {
            error(apiCallback, answer);
        }
    }

    private boolean isOk(JSONObject answer) {
        try {
            return answer != null && "ok".equals(answer.getString("status"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Background
    public void logout(APICallback apiCallback) {
        String url = getUri("wot")
                .appendPath("auth")
                .appendPath("logout")
                .appendPath("")
                .build().toString();

        JSONObject answer = requestPost(url, getDefaultParams());
        if (isOk(answer)) {
            success(apiCallback, answer);
        } else {
            error(apiCallback, answer);
        }
    }

    @UiThread
    protected void success(APICallback apiCallback, JSONObject result) {
        if (result != null && result.has("data")) {
            try {
                apiCallback.onSuccess(result.getJSONObject("data"));
            } catch (JSONException e) {
                e.printStackTrace();
                apiCallback.onError(result);
            }
        } else {
            apiCallback.onSuccess(result);
        }
        decreaseCounter();
    }

    @UiThread
    protected void error(APICallback apiCallback, JSONObject error) {
        if (error != null && error.has("data")) {
            try {
                apiCallback.onError(error.getJSONObject("data"));
            } catch (JSONException e) {
                apiCallback.onError(error);
            }
        } else {
            try {
                if (error != null) {
                    error = error.getJSONObject("error");
                }
                apiCallback.onError(error);
            } catch (JSONException e) {
                apiCallback.onError(error);
            }
        }
        decreaseCounter();
    }

    public interface APICallback {
        public void onSuccess(JSONObject result);

        public void onError(JSONObject error);
    }
}

package com.appgranula.ingame.fragments;

import android.text.Spanned;

import com.appgranula.ingame.R;

import org.androidannotations.annotations.EFragment;

/**
 * Created by zaq on 26.09.2014.
 */
@EFragment(R.layout.fragment_error)
public class ErrorFragment extends MainActivityFragment{
    @Override
    public String getFragmentSubTitle() {
        return null;
    }

    @Override
    public Spanned getFragmentTitle() {
        return null;
    }

    @Override
    public void notifyDataSetChanged() {

    }

    @Override
    public void parseError(Object e) {

    }

    @Override
    public String getFragmentTag() {
        return null;
    }

    @Override
    public boolean isAddToBackStack() {
        return false;
    }
}

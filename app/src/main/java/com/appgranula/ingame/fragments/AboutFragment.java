package com.appgranula.ingame.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;
import android.widget.TextView;

import com.appgranula.ingame.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Belozerov on 19.09.2014.
 */
@EFragment(R.layout.fragment_about)
public class AboutFragment extends MainActivityFragment {
    private static final String TAG = "AboutFragment";
    @ViewById
    TextView rateContest;
    @ViewById
    TextView copyright;
    @ViewById
    TextView wgdcContest;
    @ViewById
    TextView appVersion;

    @Override
    public String getFragmentSubTitle() {
        return null;
    }

    @Override
    public Spanned getFragmentTitle() {
        return Html.fromHtml(getString(R.string.aboutApp));
    }

    @Override
    public void notifyDataSetChanged() {

    }

    @Override
    public void parseError(Object e) {

    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public boolean isAddToBackStack() {
        return false;
    }

    @AfterViews
    void init() {
        rateContest.setText(Html.fromHtml(getString(R.string.aboutRateUs)));
        copyright.setText(Html.fromHtml(getString(R.string.copyright)));
        wgdcContest.setText(Html.fromHtml(getString(R.string.wgdcContest)));
        try {
            appVersion.setText(getString(R.string.appVersion, getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Click
    void appgranula() {
        openUrl(getString(R.string.appgranulaUrl));
    }

    @Click
    void copyright() {
        openUrl(getString(R.string.copyrightUrl));
    }

    @Click
    void wgdcContest() {
        openUrl(getString(R.string.wgdcContestUrl));
    }

    @Click
    void rateContest() {
        openUrl(getString(R.string.rateContestUrl));
    }

    private void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}

package com.appgranula.ingame.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.utils.Utils;
import com.appgranula.ingame.widget.adapter.GroupMemberAdapter;
import com.appgranula.ingame.wot.WargamingAPI;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import ch.lambdaj.Lambda;
import ch.lambdaj.function.matcher.Predicate;

/**
 * Created by Belozerov on 28.08.2014.
 */
@EFragment(R.layout.fragment_add_group)
public class AddGroupMembersFragment extends MainActivityFragment implements SearchView.OnQueryTextListener {
    public static final String TAG = "AddGroupFragment";
    private static final String SELECTED_FRIENDS = "SelectedFriends";
    @ViewById
    ListView chooseFriendList;
    @ViewById
    TextView emptyList;
    @FragmentArg
    boolean isEdit = false;
    @FragmentArg
    String game = WargamingAPI.WOT;
    @FragmentArg
    boolean isClan = false;
    GroupMemberAdapter groupMemberAdapter;
    ArrayList<String> selected;
    SearchView searchView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            selected = savedInstanceState.getStringArrayList(SELECTED_FRIENDS);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            selected = savedInstanceState.getStringArrayList(SELECTED_FRIENDS);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (groupMemberAdapter != null)
            outState.putStringArrayList(SELECTED_FRIENDS, groupMemberAdapter.getSelected());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isEdit) {
            inflater.inflate(R.menu.menu_add_members, menu);
        } else {
            inflater.inflate(R.menu.manu_new_group, menu);
        }
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.searchHint));
        searchView.setOnQueryTextListener(this);
        Utils.styleSearchView(searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuNext) {
            goNext();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goNext() {
        this.selected = groupMemberAdapter.getSelected();
        activityCallback.replaceFragment(
                EditGroupFragment_.builder().isEdit(false).isClan(isClan).game(game).groupUsers(selected).build());
    }

    @Override
    public void onPause() {
        super.onPause();
        if(activityCallback.getCurrentUser(game) == null)
            return;
        this.selected = groupMemberAdapter.getSelected();
        WotGroup wotGroup = getWotGroup();
        if (isEdit && wotGroup != null) {

            ArrayList<WotUser> removed = new ArrayList<WotUser>();
            ArrayList<WotUser> added = new ArrayList<WotUser>();
            for (WotUser wotUser : wotGroup.members) {
                if (!this.selected.contains(wotUser.nickname)) {
                    removed.add(wotUser);
                }
            }
            for (WotUser wotUser : activityCallback.getFriends()) {
                if (this.selected.contains(wotUser.nickname)) {
                    added.add(wotUser);
                }
            }
            wotGroup.members.clear();
            wotGroup.members.addAll(added);
            wotGroup.removedMembers.addAll(removed);
        }
    }

    @AfterViews
    void afterViews() {
        notifyDataSetChanged();
    }

    @Override
    public String getFragmentSubTitle() {
        return null;
    }

    @Override
    public Spanned getFragmentTitle() {
        if (getWotGroup() != null)
            return Html.fromHtml(getString(R.string.addMembers));
        return Html.fromHtml(getString(R.string.newGroup));
    }

    private WotGroup getWotGroup() {
        return activityCallback.getCurrentGroup();
    }

    private List<WotUser> getFriendsWithoutGroups() {
        List<WotUser> allFriends = activityCallback.getFriends();
        final String searchText = searchView.getQuery().toString();
        return Lambda.filter(new Predicate<WotUser>() {
            @Override
            public boolean apply(WotUser wotUser) {
                return !wotUser.isGroup() && wotUser.nickname.toLowerCase().contains(searchText.toLowerCase());
            }
        }, allFriends);
    }

    @Override
    @UiThread
    public void notifyDataSetChanged() {
        if (getActivity() == null)
            return;
        if ((isEdit && getWotGroup() == null) || activityCallback.getCurrentUser(game) == null) {
            closeFragment();
            return;
        }
        if (chooseFriendList == null)
            return;
        groupMemberAdapter = new GroupMemberAdapter(getActivity(), getFriendsWithoutGroups());
        if (selected != null)
            groupMemberAdapter.setSelected(selected);

        WotGroup wotGroup = getWotGroup();
        if (wotGroup != null) {
            ArrayList<String> alreadyInGroup = wotGroup.getMembersArray();
            groupMemberAdapter.setSelected(alreadyInGroup);
        }

        chooseFriendList.setAdapter(groupMemberAdapter);
        chooseFriendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String nickname = groupMemberAdapter.getItem(i).nickname;
                List<String> selected = groupMemberAdapter.getSelected();
                if (selected.contains(nickname)) {
                    selected.remove(nickname);
                } else {
                    selected.add(nickname);
                }
                groupMemberAdapter.notifyDataSetChanged();
            }
        });
        if (groupMemberAdapter.getCount() == 0) {
            showEmptyList();
        } else {
            hideEmptyList();
        }
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void parseError(Object e) {

    }

    private void showEmptyList() {
        if (!TextUtils.isEmpty(searchView.getQuery().toString())) {
            emptyList.setText(getString(R.string.noFriendsFound, searchView.getQuery().toString()));
        } else {
            emptyList.setText(R.string.noFriendsText);
        }
        emptyList.setVisibility(View.VISIBLE);
        chooseFriendList.setVisibility(View.GONE);
    }

    private void hideEmptyList() {
        emptyList.setVisibility(View.GONE);
        chooseFriendList.setVisibility(View.VISIBLE);
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public boolean isAddToBackStack() {
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (groupMemberAdapter != null) {
            List<WotUser> users = getFriendsWithoutGroups();
            if (users.size() == 0) {
                showEmptyList();
            } else {
                hideEmptyList();
            }
            groupMemberAdapter.setUsers(users);
            groupMemberAdapter.notifyDataSetChanged();
        }
        return true;
    }
}

package com.appgranula.ingame.utils;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.widget.SearchView;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.appgranula.ingame.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Belozerov on 14.08.2014.
 */
public class Utils {
    @TargetApi(13)
    public static Point getDisplaySize(Context context) {
        Point point = new Point();
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 13) {
            display.getSize(point);
        } else {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    public static Long truncToDay(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTimeInMillis();
        } else {
            return null;
        }
    }

    public static void styleSearchView(SearchView searchView) {
        EditText searchEdit = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEdit.setTextColor(searchView.getResources().getColor(R.color.titleTextColor));
        searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn).setBackgroundResource(R.drawable.ingame_list_selector_holo_light);
    }

    public static void styleAlertDialog(AlertDialog dialog) {
        try {
            Resources resources = dialog.getContext().getResources();

            int alertTitleId = resources.getIdentifier("alertTitle", "id", "android");
            TextView alertTitle = (TextView) dialog.getWindow().getDecorView().findViewById(alertTitleId);
            alertTitle.setTextColor(resources.getColor(R.color.titleTextColor));

//            int alertMessageId = resources.getIdentifier("alertMessage", "id", "android");
//            TextView alertMessage = (TextView) dialog.getWindow().getDecorView().findViewById(alertMessageId);
//            alertMessage.setTextColor(resources.getColor(R.color.titleTextColor));
//            alertMessage.setTextSize(resources.getDimensionPixelSize(R.dimen.dialogTextSize));

            int titleDividerId = resources.getIdentifier("titleDivider", "id", "android");
            View titleDivider = dialog.getWindow().getDecorView().findViewById(titleDividerId);
            titleDivider.setBackgroundColor(resources.getColor(R.color.ingameSendingColor));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static int nthOccurrence(String str, char c, int n) {
        int pos = str.indexOf(c, 0);
        while (n-- > 0 && pos != -1)
            pos = str.indexOf(c, pos+1);
        return pos;
    }
}

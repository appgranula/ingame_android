package com.appgranula.ingame.wot;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

import com.appgranula.ingame.R;
import com.appgranula.ingame.prefs.UserData_;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Belozerov on 29.08.2014.
 */
public class WotGroup extends WotUser implements Serializable {
    public ArrayList<WotUser> members = new ArrayList<WotUser>();
    public ArrayList<WotUser> removedMembers = new ArrayList<WotUser>();
    public String oldName;

    public int getOnlineCount() {
        int count = 0;
        if (members != null) {
            for (WotUser member : members) {
                if (member.isOnline()) {
                    count++;
                }
            }
        }
        return count;
    }

    public String getOnlineCountString(Resources context) {
        return context.getString(R.string.groupOnline, getOnlineCount(), getCount());
    }

    public static String getGroupField(String game, boolean isClan){
        return game + (isClan?"Clan":"") + "Groups";
    }

    @Override
    public boolean hasIngame() {
        return true;
    }

    @Override
    public boolean isGroup() {
        return true;
    }

    public int getCount() {
        return members == null ? 0 : members.size();
    }

    public ArrayList<String> getFormattedMembersArray(UserData_ userData, boolean oldName) {
        if (members == null)
            return new ArrayList<String>();
        ArrayList<String> formatted = new ArrayList<String>(members.size());
        for (WotUser member : members) {
            formatted.add(member.getRegionNickName(userData) + "." + (oldName ? this.oldName : nickname));
        }
        return formatted;
    }

    @Override
    public String getRegionNickName(UserData_ userData) {
        return "";
    }

    @Override
    public boolean isOnline() {
        return true;
    }

    @Override
    public String getLastBattleTime(Resources resources) {
        return "";
    }

    @Override
    public JSONObject getPushObject(Context context) {
        return new JSONObject();
    }

    public ArrayList<String> getMembersArray() {
        ArrayList<String> users = new ArrayList<String>();
        for (WotUser wotUser : members) {
            users.add(wotUser.nickname);
        }
        return users;
    }

    public ArrayList<String> getFormattedRemovedMembersArray(UserData_ userData) {
        if (removedMembers == null)
            return new ArrayList<String>();
        ArrayList<String> formatted = new ArrayList<String>(removedMembers.size());
        for (WotUser member : removedMembers) {
            formatted.add(member.getRegionNickName(userData) + "." + (TextUtils.isEmpty(oldName) ? nickname : oldName));
        }
        return formatted;
    }
}

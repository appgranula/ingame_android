package com.appgranula.ingame.parse;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.appgranula.ingame.R;
import com.appgranula.ingame.prefs.UserData_;
import com.appgranula.ingame.utils.Log_;
import com.appgranula.ingame.wot.WargamingAPI;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Belozerov on 22.08.2014.
 */
@EBean
public class ParseAPI {
    @Pref
    UserData_ userData;

    private void runCloudFunction(final Context context, String name, HashMap<String, Object> params, final FunctionCallback<Object> callback) {
        ParseCloud.callFunctionInBackground(name, params, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                if (e != null) {
                    parseError(context, e);
                }
                callback.done(o, e);
            }
        });
    }

    private void parseError(Context context, ParseException e) {
        if (context == null)
            return;
        String message = context.getString(R.string.network_error);
        if (e != null && e.getCode() != ParseException.CONNECTION_FAILED)
            message = e.getMessage();
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void logIn(final Context context, String username, String pwd, final LogInCallback callback) {
        ParseUser.logInInBackground(username, pwd, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e != null) {
                    parseError(context, e);
                }
                callback.done(parseUser, e);
            }
        });
    }

    public void wargamingAuth(Context context, FunctionCallback<Object> callback) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("accessToken", userData.accessToken().get());
        params.put("accountId", userData.accountId().get());
        params.put("nickname", userData.nickname().get());
        params.put("game", "wot");
        params.put("region", userData.region().get());
        runCloudFunction(context, "wargamingAuth", params, callback);
    }

    public void saveNicknameIntoInstallation(final Context context, final SaveCallback saveCallback) {
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        parseInstallation.put("wotNickname", WotUser.getRegionNickName(userData.nickname().get(), userData.region().get()));
        parseInstallation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    parseError(context, e);
                }
                saveCallback.done(e);
            }
        });
    }

    public boolean checkInstallation() {
        Log_.i("Check installation");
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        try {
            parseInstallation.fetch();
            if (WotUser.getRegionNickName(userData.nickname().get(), userData.region().get()).equals(parseInstallation.get("wotNickname"))) {
                Log_.i("Installation successfully checked");
                parseInstallation.put("lastSeenAt", new Date());
                parseInstallation.saveEventually();
                return true;
            } else {
                Log_.w("Installation check failed: " + parseInstallation.get("wotNickname") + " not equal " + (WotUser.getRegionNickName(userData.nickname().get(), userData.region().get())));
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Log_.w("Installation check failed");
            return true;
        }
    }

    public boolean sendIngame(Context context, WotUser wotUser, WotUser currentWotUser, String game) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        ArrayList<String> nicknames = new ArrayList<String>();
        if (wotUser instanceof WotGroup) {
            for (WotUser member : ((WotGroup) wotUser).members) {
                nicknames.add(member.getRegionNickName(userData));
            }
        } else {
            nicknames.add(wotUser.getRegionNickName(userData));
        }
        params.put("nicknames", nicknames);
        params.put("game", game);
        params.put("message", currentWotUser.getPushMessage(context, game));
        try {
            ParseCloud.callFunction("sendPush", params);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }


    public void logout(Context context, FunctionCallback<Object> callback) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("game", "wot");
        params.put("region", userData.region().get());
        runCloudFunction(context, "logout", params, callback);
    }

    public void saveGroup(WotGroup group, String game, boolean isClan) {
        ParseUser current = ParseUser.getCurrentUser();
        if (current != null) {
            ArrayList<String> groups = (ArrayList<String>) current.get(WotGroup.getGroupField(game, isClan));
            if (groups == null) {
                groups = new ArrayList<String>();
            }
            groups.removeAll(group.getFormattedRemovedMembersArray(userData));
            groups.removeAll(group.getFormattedMembersArray(userData, true));
            groups.addAll(group.getFormattedMembersArray(userData, false));
            current.put(WotGroup.getGroupField(game, isClan), groups);
            current.saveEventually();
        }
    }

    public void removeGroup(WotGroup group, String game, boolean isClan) {
        ParseUser current = ParseUser.getCurrentUser();
        if (current != null) {
            current.removeAll(WotGroup.getGroupField(game, isClan), group.getFormattedMembersArray(userData, false));
            current.saveEventually();
        }
    }

    public void startWatching(WotUser user) {
        ParseUser current = ParseUser.getCurrentUser();
        ArrayList<String> watchList = (ArrayList<String>) current.get("watchList");
        if (watchList == null)
            watchList = new ArrayList<String>();
        watchList.add(user.getRegionNickName(userData));
        current.put("watchList", watchList);
        current.saveEventually();
    }

    public void stopWatching(WotUser user) {
        ParseUser current = ParseUser.getCurrentUser();
        ArrayList<String> watchList = (ArrayList<String>) current.get("watchList");
        if (watchList == null)
            watchList = new ArrayList<String>();
        watchList.remove(user.getRegionNickName(userData));
        current.put("watchList", watchList);
        current.saveEventually();
    }

    public void saveAllowedFriends(List<WotUser> users, String game, boolean isClan) {
        if (users == null)
            return;

        List<String> userNames = new ArrayList<String>();
        String nickname = "";
        for (WotUser user : users) {
            nickname = user.getRegionNickName(userData);
            if(!TextUtils.isEmpty(nickname)) {
                userNames.add(nickname);
            }
        }
        String field = "wotFriends";
        if (isClan) {
            field = "wotClanMembers";
        } else if (WargamingAPI.WOWP.equals(game)) {
            field = "wowpFriends";
        }
        ParseUser.getCurrentUser().put(field, userNames);
        ParseUser.getCurrentUser().saveEventually();
    }
}

package com.appgranula.ingame.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Belozerov on 15.08.2014.
 */
public class SwipeRefreshLayoutListViewFix extends SwipeRefreshLayout {
    public SwipeRefreshLayoutListViewFix(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwipeRefreshLayoutListViewFix(Context context) {
        super(context);
    }

    private StickyListHeadersListView listView;

    public void setListView(StickyListHeadersListView listView) {
        this.listView = listView;
    }

    @Override
    public boolean canChildScrollUp() {
        if (listView == null) {
            return super.canChildScrollUp();
        }
        View c = listView.getWrappedList().getChildAt(0);
        if (c == null) {
            return super.canChildScrollUp();
        }
        return -c.getTop() + listView.getFirstVisiblePosition() * c.getHeight() != 0;
    }
}

package com.appgranula.ingame.fragments;

import android.graphics.PorterDuff;
import android.support.v4.app.Fragment;
import android.widget.ProgressBar;

import com.appgranula.ingame.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Belozerov on 28.08.2014.
 */
@EFragment(R.layout.fragment_loading)
public class LoadingFragment extends Fragment {
    public static final String TAG = "LoadingFragment";
    @ViewById
    ProgressBar progressBar;

    @AfterViews
    void init() {
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ingameSendingColor), PorterDuff.Mode.MULTIPLY);
    }
}

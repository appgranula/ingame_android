package com.appgranula.ingame.prefs;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Belozerov on 11.08.2014.
 */
@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface UserData {
    public String accessToken();

    public String nickname();

    public String accountId();

    public long expiresAt();

    public String region();
}

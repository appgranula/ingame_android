package com.appgranula.ingame.fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appgranula.ingame.MainActivity_;
import com.appgranula.ingame.R;
import com.appgranula.ingame.StartActivity;
import com.appgranula.ingame.parse.ParseAPI;
import com.appgranula.ingame.prefs.UserData_;
import com.appgranula.ingame.wot.WargamingAPI;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.HashMap;

/**
 * Created by Belozerov on 19.08.2014.
 */
@EFragment(R.layout.fragment_auth)
public class AuthFragment extends Fragment {
    public static final String TAG = "AuthFragment";
    @ViewById
    ProgressBar progressBar;
    @ViewById
    WebView authWebView;
    @Pref
    UserData_ userData;
    @Bean
    WargamingAPI wargamingAPI;
    @Bean
    ParseAPI parseAPI;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void goToMain() {
        Intent intent = new Intent(getActivity(), MainActivity_.class);
        if (Build.VERSION.SDK_INT >= 11)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ((StartActivity) getActivity()).showRegionFragment();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @AfterViews
    void afterViews() {
        ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ProgressDialogFragment.show(getActivity());
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ingameSendingColor), PorterDuff.Mode.MULTIPLY);
        authWebView.getSettings().setJavaScriptEnabled(true);
        authWebView.clearCache(true);
        authWebView.setWebViewClient(new WargamingWebViewClient());

        CookieSyncManager.createInstance(getActivity());

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        authWebView.loadUrl(createAuthUrl());
    }

    private String createAuthUrl() {
        return wargamingAPI.getUri(WargamingAPI.WOT)
                .appendPath("auth")
                .appendPath("login")
                .appendPath("")
                .appendQueryParameter("display", "popup")
                .appendQueryParameter("application_id", wargamingAPI.getAppId())
                .appendQueryParameter("redirect_uri", WargamingAPI.REDIRECT_URI).toString();
    }

    public WebView getWebView() {
        return authWebView;
    }

    public class WargamingWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            parseUrl(url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (!isCallBackUrl(url)) {
                ProgressDialogFragment.hide(getActivity());
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            view.loadUrl("about:blank");
            if (getActivity() != null) {
                Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_LONG).show();
            }
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    webViewReset();
                }
            }, 1000);
        }

        private void parseUrl(String url) {
            if (isCallBackUrl(url)) {
                if (url.contains("status=ok")) {
                    Uri uri = Uri.parse(url);
                    userData.expiresAt().put(Long.parseLong(uri.getQueryParameter("expires_at")));
                    userData.accessToken().put(uri.getQueryParameter("access_token"));
                    userData.accountId().put(uri.getQueryParameter("account_id"));
                    userData.nickname().put(uri.getQueryParameter("nickname"));
                    parseWargamingAuth();
                } else {
                    webViewReset();
                }
            }
        }


        private boolean isCallBackUrl(String url) {
            return url != null && url.startsWith(WargamingAPI.REDIRECT_URI);
        }
    }

    private void webViewReset() {
        if (getActivity() == null)
            return;
        ((StartActivity) getActivity()).showRegionFragment();
    }

    private void parseWargamingAuth() {
        ProgressDialogFragment.show(getActivity());
        if (ParseUser.getCurrentUser() != null)
            ParseUser.logOut();
        parseAPI.wargamingAuth(getActivity(), new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                if (e == null) {
                    HashMap userCredentials = (HashMap) o;
                    parseLogin(userCredentials);
                } else
                    parseError();
            }
        });
    }

    private void parseError() {
        ProgressDialogFragment.hide(getActivity());
        webViewReset();
    }

    private void parseLogin(HashMap userCredentials) {
        String username = (String) userCredentials.get("username");
        String pwd = (String) userCredentials.get("password");
        parseAPI.logIn(getActivity(), username, pwd, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e == null)
                    saveInstallation();
                else
                    parseError();
            }
        });
    }

    private void saveInstallation() {
        parseAPI.saveNicknameIntoInstallation(getActivity(), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null)
                    goToMain();
                else
                    parseError();
            }
        });
    }
}

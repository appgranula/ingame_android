package com.appgranula.ingame.widget.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.widget.ClanView_;
import com.appgranula.ingame.wot.WotClan;
import com.appgranula.ingame.wot.WotUser;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belozerov on 12.09.2014.
 */
@EBean
public class DrawerAdapter extends BaseAdapter {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_CLAN = 2;
    private LayoutInflater layoutInflater;
    @RootContext
    Context context;
    List<DrawerItem> drawerItemList = new ArrayList<DrawerItem>();

    @UiThread
    public void init(WotUser wotUser, WotClan clan, WotUser wowpUser) {
        drawerItemList.clear();
        if(wotUser != null) {
            drawerItemList.add(new DrawerHeaderItem(R.string.worldOfTanksGroup));
            drawerItemList.add(new DrawerItem(R.string.friends, R.drawable.ic_action_social_cc_bcc, R.id.menuDrawerItemWotFriends));
            if (clan != null)
                drawerItemList.add(new DrawerClanItem(clan, R.id.menuDrawerItemWotClan));
        }
        if(wowpUser != null){
            drawerItemList.add(new DrawerHeaderItem(R.string.worldOfWarPlanesGroup));
            drawerItemList.add(new DrawerItem(R.string.friends, R.drawable.ic_action_social_cc_bcc, R.id.menuDrawerItemWowpFriends));
        }
        drawerItemList.add(new DrawerHeaderItem(R.string.general));
        drawerItemList.add(new DrawerItem(R.string.feedback, R.drawable.ic_action_content_email, R.id.menuDrawerItemFeedback));
        drawerItemList.add(new DrawerItem(R.string.rateUs, R.drawable.ic_action_rating_good, R.id.menuDrawerItemRate));
        drawerItemList.add(new DrawerItem(R.string.aboutApp, R.drawable.ic_action_action_about, R.id.menuDrawerItemAbout));
        drawerItemList.add(new DrawerItem(R.string.logout, R.drawable.ic_action_icon_logout, R.id.menuDrawerItemLogout));
        notifyDataSetChanged();
    }

    @AfterInject
    public void afterInject(){
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return drawerItemList.size();
    }

    @Override
    public DrawerItem getItem(int i) {
        return drawerItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        DrawerItem drawerItem = getItem(position);
        if(drawerItem instanceof DrawerClanItem)
            return TYPE_CLAN;
        if (drawerItem instanceof DrawerHeaderItem)
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        switch (getItemViewType(i)){
            case TYPE_ITEM:
                return getItemView(i, view, viewGroup);
            case TYPE_HEADER:
                return getHeaderView(i, view, viewGroup);
            case TYPE_CLAN:
                return getClanView(i, view, viewGroup);

        }
        return null;
    }

    private View getClanView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = ClanView_.build(context);
        }
        ClanView_ clanView = (ClanView_) view;
        clanView.bind((DrawerClanItem) getItem(i));
        return clanView;
    }

    private View getHeaderView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = layoutInflater.inflate(R.layout.item_menu_header, viewGroup, false);
        }
        ((TextView)((ViewGroup)view).getChildAt(0)).setText(((DrawerHeaderItem)getItem(i)).text);
        return view;
    }

    private View getItemView(int i, View view, ViewGroup viewGroup) {
        ViewHolderItem viewHolderItem;
        if(view == null){
            view = layoutInflater.inflate(R.layout.item_menu, viewGroup, false);
            viewHolderItem = new ViewHolderItem();
            viewHolderItem.icon = (ImageView) view.findViewById(R.id.menuItemIcon);
            viewHolderItem.text = (TextView) view.findViewById(R.id.menuItemText);
            view.setTag(viewHolderItem);
        } else {
            viewHolderItem = (ViewHolderItem) view.getTag();
        }
        DrawerItem drawerItem = (DrawerItem) getItem(i);
        viewHolderItem.text.setText(drawerItem.text);
        viewHolderItem.icon.setImageResource(drawerItem.icon);
        return view;
    }

    private static class ViewHolderItem {
        TextView text;
        ImageView icon;
    }

    public static class DrawerItem {
        public int text;
        public boolean isHeader;
        public int icon;
        public int id;

        public DrawerItem(int text, int icon, int id) {
            this.text = text;
            this.icon = icon;
            this.id = id;
            this.isHeader = false;
        }

        public DrawerItem(int text, int icon, int id, boolean isHeader) {
            this.text = text;
            this.icon = icon;
            this.id = id;
            this.isHeader = isHeader;
        }
    }

    public static class DrawerClanItem extends DrawerItem {
        public WotClan clan;
        public DrawerClanItem(WotClan clan, int id) {
            super(0, 0, id, false);
            this.clan = clan;
        }
    }

    public static class DrawerHeaderItem extends DrawerItem {

        public DrawerHeaderItem(int text) {
            super(text, 0, 0, true);
        }
    }
}

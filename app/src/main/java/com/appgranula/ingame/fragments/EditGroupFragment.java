package com.appgranula.ingame.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.appgranula.ingame.R;
import com.appgranula.ingame.parse.ParseAPI;
import com.appgranula.ingame.utils.Utils;
import com.appgranula.ingame.widget.adapter.FriendsAdapter;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Belozerov on 28.08.2014.
 */
@EFragment(R.layout.fragment_edit_group)
public class EditGroupFragment extends MainActivityFragment {
    private static final String TAG = "EditGroupFragment";
    @ViewById
    StickyListHeadersListView membersList;
    @ViewById
    EditText groupNameEdit;
    @FragmentArg
    boolean isEdit;
    @FragmentArg
    boolean isClan;
    @FragmentArg
    String game;
    @FragmentArg
    ArrayList<String> groupUsers;
    @Bean
    ParseAPI parseAPI;
    FriendsAdapter adapter;
    List<WotUser> groupMembers = new ArrayList<WotUser>();
    private boolean isDeleted = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isEdit) {
            inflater.inflate(R.menu.menu_group_edit, menu);
        } else {
            inflater.inflate(R.menu.menu_group_create, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuCreate:
                saveGroup();
                closeFragment();
                return true;
            case R.id.menuAddMoreMembers:
                addMoreMembers();
                return true;
            case R.id.menuDeleteGroup:
                deleteGroupDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public String getFragmentSubTitle() {
        return null;
    }

    private void deleteGroupDialog() {
        View dialogView = View.inflate(getActivity(), R.layout.delete_group_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog dialog = builder
                .setTitle(R.string.deleteGroup)
                .setView(dialogView)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteGroup();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        dialog.show();
        Utils.styleAlertDialog(dialog);
    }

    private void deleteGroup() {
        WotGroup wotGroup = getWotGroup();
        parseAPI.removeGroup(wotGroup, game, isClan);
        activityCallback.removeGroup(wotGroup);
        isDeleted = true;
        closeFragment();
    }

    private void addMoreMembers() {
        AddGroupMembersFragment addGroupMembersFragment = AddGroupMembersFragment_
                .builder()
                .isEdit(true)
                .build();
        activityCallback.replaceFragment(addGroupMembersFragment);
    }


    private void saveGroup() {
        WotGroup wotGroup = getWotGroup();
        if (isEdit && wotGroup == null)
            return;
        if (wotGroup == null)
            wotGroup = new WotGroup();
        wotGroup.oldName = wotGroup.nickname;
        wotGroup.nickname = getName();
        wotGroup.members.clear();
        for (WotUser wotUser : groupMembers) {
            wotGroup.members.add(wotUser);
        }
        parseAPI.saveGroup(wotGroup, game, isClan);
        activityCallback.saveGroup(wotGroup, isEdit);
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private String getName() {
        String editGroupName = "";
        if (getWotGroup() != null) {
            editGroupName = getWotGroup().nickname;
        }
        String defaultName = groupNameEdit.getText().toString();
        if (!TextUtils.isEmpty(editGroupName) && editGroupName.equals(defaultName))
            return editGroupName;
        if (TextUtils.isEmpty(defaultName)) {
            defaultName = getString(R.string.unnamedGroup);
        }
        String name = defaultName;
        boolean foundSame;
        int counter = 1;
        do {
            foundSame = false;
            List<WotGroup> groups = activityCallback.getGroups();
            for (WotGroup group : groups) {
                if (name.equals(group.nickname)) {
                    foundSame = true;
                    name = defaultName + " " + counter++;
                    break;
                }
            }
        } while (foundSame);

        return name.trim();
    }

    @AfterViews
    void init() {
        WotGroup wotGroup = getWotGroup();
        if (wotGroup != null) {
            groupNameEdit.setText(wotGroup.nickname);
        }
        notifyDataSetChanged();
        initListDeleteItems();
    }

    private void initListDeleteItems() {
        membersList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final WotUser wotUser = adapter.getItem(i);
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1, new String[]{getString(R.string.removeMemberFromGroup, wotUser.nickname)});
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                AlertDialog dialog = builder
                        .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getWotGroup().members.remove(wotUser);
                                getWotGroup().removedMembers.add(wotUser);
                                parseAPI.saveGroup(getWotGroup(), game, isClan);
                                notifyDataSetChanged();
                            }
                        })
                        .setCancelable(true)
                        .create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                return true;
            }
        });
    }

    private WotGroup getWotGroup() {
        return activityCallback.getCurrentGroup();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(activityCallback.getCurrentUser(game) == null)
            return;
        if (isEdit && !isDeleted)
            saveGroup();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(groupNameEdit.getWindowToken(), 0);
    }

    @Override
    public Spanned getFragmentTitle() {
        WotGroup wotGroup = getWotGroup();
        if (wotGroup != null)
            return Html.fromHtml(wotGroup.nickname);
        return Html.fromHtml(getString(R.string.newGroup));
    }

    @Override
    @UiThread
    public void notifyDataSetChanged() {
        if (getActivity() == null)
            return;
        adapter = new FriendsAdapter(getActivity());
        WotGroup wotGroup = getWotGroup();
        if ((isEdit && wotGroup == null) || activityCallback.getCurrentUser(game) == null)
            closeFragment();
        if (wotGroup != null)
            groupUsers = wotGroup.getMembersArray();
        groupMembers.clear();
        for (WotUser wotUser : activityCallback.getFriends()) {
            if (groupUsers.contains(wotUser.nickname)) {
                groupMembers.add(wotUser);
            }
        }
        adapter.setUserList(groupMembers);
        membersList.setAdapter(adapter);
    }

    @Override
    public void parseError(Object e) {

    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public boolean isAddToBackStack() {
        return true;
    }
}

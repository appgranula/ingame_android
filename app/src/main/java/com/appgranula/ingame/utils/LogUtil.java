package com.appgranula.ingame.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

import com.appgranula.ingame.R;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;

/**
 * User: beloz_000
 * Date: 02.08.13
 * Time: 18:33
 */
public class LogUtil {
    public static void sendEmail(Context context) {
        final String email = context.getString(R.string.support_email);
        final String subject = context.getString(R.string.support_subject);
        Intent emailIntent;
        emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        uris.add(Log_.getUri());
        uris.add(getDeviceInfo(context));
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        try {
            context.startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
        }
    }

    public static Uri getDeviceInfo(Context context) {
        OutputStream outputStream = null;
        File outputFile = new File(context.getExternalFilesDir(null).toString() + "/" + "deviceInfo.txt");
        try {
            outputStream = new FileOutputStream(outputFile);
            String versionName = "";
            String versionCode = "";
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                versionCode += packageInfo.versionCode;
                versionName = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String deviceName =
                    "Android Device: " + Build.MODEL
                            + " API version: " + Build.VERSION.SDK_INT + "\n"
                            + " VersionCode: " + versionCode
                            + " VersionName: " + versionName + "\n"
                            + " Locale:" + Locale.getDefault().getDisplayName()
                            + " ParseUser: " + (ParseUser.getCurrentUser() != null ? ParseUser.getCurrentUser().getObjectId() : "none");
            byte[] deviceNameArray = deviceName.getBytes();
            outputStream.write(deviceNameArray, 0, deviceNameArray.length);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Uri.fromFile(outputFile);
    }

}

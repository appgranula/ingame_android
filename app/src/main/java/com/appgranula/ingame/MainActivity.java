package com.appgranula.ingame;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.appgranula.ingame.fragments.AboutFragment_;
import com.appgranula.ingame.fragments.ErrorFragment_;
import com.appgranula.ingame.fragments.FriendsListFragment;
import com.appgranula.ingame.fragments.FriendsListFragment_;
import com.appgranula.ingame.fragments.LoadingFragment;
import com.appgranula.ingame.fragments.LoadingFragment_;
import com.appgranula.ingame.fragments.MainActivityFragment;
import com.appgranula.ingame.fragments.ProgressDialogFragment;
import com.appgranula.ingame.parse.ParseAPI;
import com.appgranula.ingame.prefs.AllowedUsers_;
import com.appgranula.ingame.prefs.UserData_;
import com.appgranula.ingame.utils.LogUtil;
import com.appgranula.ingame.utils.Log_;
import com.appgranula.ingame.utils.TypefaceSpan;
import com.appgranula.ingame.widget.adapter.DrawerAdapter;
import com.appgranula.ingame.wot.WargamingAPI;
import com.appgranula.ingame.wot.WotClan;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;
import com.parse.FunctionCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belozerov on 11.08.2014.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends ActionBarActivity implements MainActivityFragment.MainActivityInterface, AdapterView.OnItemClickListener {
    private static final String CURRENT_FRAGMENT = "current_fragment";

    @ViewById
    ListView leftDrawer;
    @ViewById
    DrawerLayout drawerLayout;
    @Pref
    UserData_ userData;
    @Bean
    WargamingAPI wargamingAPI;
    @Bean
    ParseAPI parseAPI;
    @Bean
    DrawerAdapter drawerAdapter;

    boolean isError = false;

    List<WotGroup> groups = new ArrayList<WotGroup>();
    @Pref
    AllowedUsers_ allowedUsers;
    private List<WotUser> wotFriends = new ArrayList<WotUser>();
    private WotClan wotClan;
    private WotUser currentWotUser;
    private WotUser currentWowpUser;
    private WotGroup currentGroup = null;
    private int loaderCount = 0;
    private ActionBarDrawerToggle drawerToggle;
    private String currentFragmentTag = FriendsListFragment.TAG;
    private boolean isDestroyed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isDestroyed = false;
        if (savedInstanceState == null) {
            //replaceFragment(FriendsListFragment_.builder().game(WargamingAPI.WOT).build());
            showLoading();
        } else {
            currentFragmentTag = savedInstanceState.getString(CURRENT_FRAGMENT);
        }
    }

    @Override
    public void replaceFragment(MainActivityFragment fragment) {
        if (isDestroyed)
            return;
        if (getCurrentFragment() instanceof FriendsListFragment) {
            ((FriendsListFragment) getCurrentFragment()).clearSearch();
        }
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (fragment.isAddToBackStack())
            transaction.addToBackStack(fragment.getFragmentTag());
        transaction
                .replace(R.id.mainFragmentContainer, fragment, fragment.getFragmentTag())
                .commitAllowingStateLoss();
    }

    @Override
    protected void onDestroy() {
        isDestroyed = true;
        super.onDestroy();
    }

    @Override
    public List<WotGroup> getGroups() {
        return groups;
    }

    @Override
    public void saveGroup(WotGroup wotGroup, boolean isEdit) {
        if (!isEdit) {
            groups.add(wotGroup);
            wotFriends.add(wotGroup);
        }
        notifyDataSetChanged();
    }

    @Override
    public void removeGroup(WotGroup wotGroup) {
        groups.remove(wotGroup);
        wotFriends.remove(wotGroup);
        notifyDataSetChanged();
    }

    @Override
    public WotGroup getCurrentGroup() {
        return currentGroup;
    }

    @Override
    public void setCurrentGroup(WotGroup currentGroup) {
        this.currentGroup = currentGroup;
    }

    @Override
    public boolean isDrawerOpened() {
        return drawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    @Override
    public void setCurrentFriends(List<WotUser> wotFriends) {
        this.wotFriends = wotFriends;
    }

    @Override
    public WotClan getClan(String game) {
        return wotClan;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CURRENT_FRAGMENT, currentFragmentTag);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentFragmentTag = savedInstanceState.getString(CURRENT_FRAGMENT);
    }

    @AfterViews
    void afterViews() {
        prolongateAccessTokenIfNeeded();
        initDrawer();
    }

    private void increaseLoaderCount() {
        loaderCount++;
    }

    private void decreaseLoaderCount() {
        loaderCount--;
        checkAllLoad();
    }

    private void requestWowpUser() {
        increaseLoaderCount();
        wargamingAPI.requestUserInfo(userData.accountId().get(), WargamingAPI.WOWP, new WargamingAPI.APICallback() {
            @Override
            public void onSuccess(JSONObject result) {
                if (result == null) {
                    decreaseLoaderCount();
                    return;
                }
                try {
                    if (result.isNull(userData.accountId().get())) {
                        decreaseLoaderCount();
                        return;
                    }
                    JSONObject userInfo = result.getJSONObject(userData.accountId().get());
                    currentWowpUser = WotUser.get(userInfo);
                    updateTitle();
                    decreaseLoaderCount();
                } catch (JSONException e) {
                    decreaseLoaderCount();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                decreaseLoaderCount();
                checkErrorForAccessToken(error);
            }
        });
    }

    private void requestWotUser() {
        isError = false;
        increaseLoaderCount();
        wargamingAPI.requestUserInfo(userData.accountId().get(), WargamingAPI.WOT, new WargamingAPI.APICallback() {
            @Override
            public void onSuccess(JSONObject result) {
                if (result == null) {
                    decreaseLoaderCount();
                    return;
                }
                try {
                    if (result.isNull(userData.accountId().get())) {
                        decreaseLoaderCount();
                        return;
                    }
                    JSONObject userInfo = result.getJSONObject(userData.accountId().get());
                    currentWotUser = WotUser.get(userInfo);
                    updateTitle();
                    decreaseLoaderCount();
                    requestWotClanInfo();
                } catch (JSONException e) {
                    decreaseLoaderCount();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                decreaseLoaderCount();
                checkErrorForAccessToken(error);
            }
        });
        requestWowpUser();
    }

    private void checkAllLoad() {
        if (isError) {
            replaceFragment(ErrorFragment_.builder().build());
            hideLoading();
            return;
        }
        if (loaderCount == 0) {
            drawerAdapter.init(currentWotUser, wotClan, currentWowpUser);

            if (getCurrentFragment() instanceof FriendsListFragment) {
                FriendsListFragment currentFragment = (FriendsListFragment) getCurrentFragment();
                if (WargamingAPI.WOT.equals(currentFragment.game)) {
                    currentFragment.requestFriendsInfo(currentWotUser.privateInfo.friends);
                } else {
                    currentFragment.requestFriendsInfo(currentWowpUser.privateInfo.friends);
                }
                updateTitle();
                return;
            }
            if (currentWotUser != null) {
                replaceFragment(FriendsListFragment_.builder().game(WargamingAPI.WOT).build());
            } else if (currentWowpUser != null) {
                replaceFragment(FriendsListFragment_.builder().game(WargamingAPI.WOWP).build());
            } else {
                hideLoading();
            }
        }
    }

    private void requestWotClanInfo() {
        if (currentWotUser.clan_id > 0) {
            increaseLoaderCount();
            wargamingAPI.requestClanInfo(currentWotUser, WargamingAPI.WOT, new WargamingAPI.APICallback() {
                @Override
                public void onSuccess(JSONObject result) {
                    try {
                        wotClan = WotClan.get(result.getJSONObject(Long.toString(currentWotUser.clan_id)), currentWotUser);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    decreaseLoaderCount();
                }

                @Override
                public void onError(JSONObject error) {
                    decreaseLoaderCount();
                    checkErrorForAccessToken(error);
                }
            });
        } else {
            allowedUsers.clan().put("");
            decreaseLoaderCount();
        }
    }

    private void checkErrorForAccessToken(JSONObject error) {
        boolean accessTokenExpired = false;
        isError = true;
        if (error != null) {
            try {
                String errorValue = error.getString("message");
                if ("INVALID_ACCESS_TOKEN".equals(errorValue)
                        || "ACCOUNT_ID_NOT_SPECIFIED".equals(errorValue)) {
                    accessTokenExpired = true;
                    forceLogout();
                }
            } catch (JSONException ignored) {
            }
        }
        if (!accessTokenExpired) {
            parseError(error);
        }
    }

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_navigation_drawer, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (getCurrentFragment() != null) {
                    getSupportActionBar().setTitle(getCurrentFragment().getFragmentTitle());
                    setSubtitle(getCurrentFragment().getFragmentSubTitle());
                }
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(getNickname());
                    setSubtitle(null);
                }
                invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.setScrimColor(0x55000000);
        drawerAdapter.init(currentWotUser, null, null);
        leftDrawer.setAdapter(drawerAdapter);
        leftDrawer.setOnItemClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setSubtitle(String subtitle) {
        if (subtitle == null) {
            getSupportActionBar().setSubtitle(null);
            return;
        }
        SpannableString s = new SpannableString(subtitle);
        s.setSpan(new TypefaceSpan(this, "Roboto-Light.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setSubtitle(s);
    }

    private CharSequence getNickname() {
        if (!TextUtils.isEmpty(userData.nickname().get())) {
            return userData.nickname().get();
        }
        return getString(R.string.app_name);
    }

    @Background
    protected void prolongateAccessTokenIfNeeded() {
        wargamingAPI.prolongateAccessTokenIfNeeded();
        requestWotUser();
        checkInstallation();
    }

    private void checkInstallation() {
        if (!parseAPI.checkInstallation()) {
            if (!TextUtils.isEmpty(userData.accountId().get())) {
                wargamingLogout();
            }
        }
    }

    private MainActivityFragment getCurrentFragment() {
        return (MainActivityFragment) getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
    }

    @Override
    public void setCurrentFragment(MainActivityFragment fragment) {
        currentFragmentTag = fragment.getFragmentTag();
        if (fragment.isAddToBackStack()) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            drawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            drawerToggle.setDrawerIndicatorEnabled(true);
        }
        updateTitle();
    }

    @UiThread
    protected void parseError(Object e) {
        hideLoading();
        if (getCurrentFragment() != null) {
            getCurrentFragment().parseError(e);
        } else {
            String message = getString(R.string.network_error);
            if (e instanceof Exception) {
                message = ((Exception) e).getMessage();
            } else if (e instanceof JSONObject) {
                try {
                    message = ((JSONObject) e).getString("message");
                } catch (JSONException ignored) {
                }
            }
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            replaceFragment(ErrorFragment_.builder().build());
        }
    }

    private void forceLogout() {
        allowedUsers.clear();
        userData.clear();
        ParseUser.logOut();
        startActivity(new Intent(MainActivity.this, StartActivity_.class));
        finish();
    }

    private void notifyDataSetChanged() {
        getCurrentFragment().notifyDataSetChanged();
        hideLoading();
    }

    private void updateTitle() {
        if (getSupportActionBar() != null && getCurrentFragment() != null) {
            getSupportActionBar().setTitle(getCurrentFragment().getFragmentTitle());
            setSubtitle(getCurrentFragment().getFragmentSubTitle());
        }
    }

    @Override
    @UiThread
    public void showLoading() {
        if (getSupportFragmentManager().findFragmentByTag(LoadingFragment.TAG) != null)
            return;
        LoadingFragment loadingFragment = LoadingFragment_.builder().build();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.loaderContainer, loadingFragment, LoadingFragment.TAG)
                .commitAllowingStateLoss();
    }

    @Override
    @UiThread
    public void hideLoading() {
        LoadingFragment loadingFragment = (LoadingFragment) getSupportFragmentManager().findFragmentByTag(LoadingFragment.TAG);
        if (loadingFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(loadingFragment)
                    .commitAllowingStateLoss();
        }
    }

    @UiThread
    @Override
    public void wargamingLogout() {
        Log_.i("Wargaming logout");
        wargamingAPI.logout(new WargamingAPI.APICallback() {
            @Override
            public void onSuccess(JSONObject result) {
                forceLogout();
                ProgressDialogFragment.hide(MainActivity.this);
            }

            @Override
            public void onError(JSONObject error) {
                forceLogout();
                ProgressDialogFragment.hide(MainActivity.this);
            }
        });
    }

    @Override
    public WotUser getCurrentUser(String game) {
        if (WargamingAPI.WOT.equals(game))
            return currentWotUser;
        return currentWowpUser;
    }

    @Override
    public List<WotUser> getFriends() {
        return wotFriends;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            getSupportFragmentManager().popBackStack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (drawerAdapter.getItem(i).id) {
            case R.id.menuDrawerItemAbout:
                showAbout();
                break;
            case R.id.menuDrawerItemWotClan:
                replaceFragment(FriendsListFragment_.builder().isClan(true).game(WargamingAPI.WOT).build());
                break;
            case R.id.menuDrawerItemFeedback:
                LogUtil.sendEmail(this);
                break;
            case R.id.menuDrawerItemWotFriends:
                replaceFragment(FriendsListFragment_.builder().game(WargamingAPI.WOT).build());
                break;
            case R.id.menuDrawerItemLogout:
                logout();
                break;
            case R.id.menuDrawerItemRate:
                rateUs();
                break;
            case R.id.menuDrawerItemWowpFriends:
                replaceFragment(FriendsListFragment_.builder().game(WargamingAPI.WOWP).build());
                break;
        }
        drawerLayout.closeDrawers();
    }

    private void rateUs() {
        String packageName = getPackageName();
        Uri uri = Uri.parse("market://details?id=" + packageName);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ignored) {
        }
    }

    private void logout() {
        ProgressDialogFragment.show(this);
        parseAPI.logout(this, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                if (e != null) {
                    ProgressDialogFragment.hide(MainActivity.this);
                } else {
                    wargamingLogout();
                }
            }
        });
    }

    private void showAbout() {
        replaceFragment(AboutFragment_.builder().build());
    }
}

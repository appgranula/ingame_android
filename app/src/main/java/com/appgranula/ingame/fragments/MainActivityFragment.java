package com.appgranula.ingame.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.Spanned;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;

import com.appgranula.ingame.MainActivity;
import com.appgranula.ingame.wot.WotClan;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;

import org.androidannotations.annotations.EFragment;

import java.util.List;

/**
 * Created by Belozerov on 28.08.2014.
 */
@EFragment
public abstract class MainActivityFragment extends Fragment {
    public interface MainActivityInterface {
        public WotUser getCurrentUser(String game);

        public List<WotUser> getFriends();

        public void setCurrentFragment(MainActivityFragment fragment);

        public void wargamingLogout();

        public void replaceFragment(MainActivityFragment fragment);

        public List<WotGroup> getGroups();

        public void saveGroup(WotGroup wotGroup, boolean isEdit);

        public void removeGroup(WotGroup wotGroup);

        public void setCurrentGroup(WotGroup currentGroup);

        public WotGroup getCurrentGroup();

        public boolean isDrawerOpened();

        public void setCurrentFriends(List<WotUser> wotFriends);

        public WotClan getClan(String game);

        public void showLoading();

        public void hideLoading();
    }

    protected void replaceFragment(MainActivityFragment mainActivityFragment){
        ((MainActivityInterface)getActivity()).replaceFragment(mainActivityFragment);
    }

    protected MainActivityInterface activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (MainActivityInterface) activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        activityCallback.setCurrentFragment(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for(int i = 0; i < menu.size(); i++){
            menu.getItem(i).setVisible(!activityCallback.isDrawerOpened());
        }
        super.onPrepareOptionsMenu(menu);
    }

    public abstract String getFragmentSubTitle();
    public abstract Spanned getFragmentTitle();

    public abstract void notifyDataSetChanged();

    public abstract void parseError(Object e);

    public abstract String getFragmentTag();

    public abstract boolean isAddToBackStack();
}

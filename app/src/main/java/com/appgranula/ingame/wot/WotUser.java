package com.appgranula.ingame.wot;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

import com.appgranula.ingame.R;
import com.appgranula.ingame.prefs.UserData_;
import com.appgranula.ingame.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Belozerov on 12.08.2014.
 */
public class WotUser implements Serializable {
    public static final String GAME = "wot";
    private static final long ONLINE_TIME = 20 * 60 * 1000;
    private static DateFormat dateFormat = DateFormat.getDateInstance();
    private static DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
    public long account_id;
    public long last_battle_time;
    public String nickname;
    public long clan_id;
    public long logout_at;
    public long global_rating;
    public boolean hasIngame = false;
    public boolean isSending = false;
    public boolean isSent = false;
    public String account_name;
    public Date lastOnline;
    public boolean isWatching = false;
    @SerializedName("private")
    public WotUserPrivate privateInfo;

    public WotUser() {

    }

    public static String arrayToString(List<WotUser> users, UserData_ userData) {
        String retVal = "";
        for (WotUser user : users) {
            if (user != null && !TextUtils.isEmpty(user.nickname)) {
                retVal += user.getRegionNickName(userData) + ",";
            }
        }
        if (TextUtils.isEmpty(retVal)) {
            return "";
        }
        return retVal.substring(0, retVal.length() - 1);
    }

    public static String getRegionNickName(String nickname, String region) {
        return region + "_" + GAME + "_" + nickname;
    }

    public static WotUser get(JSONObject jsonObject) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(jsonObject.toString(), WotUser.class);
    }

    public static WotUser get(JsonElement jsonObject) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        WotUser wotUser = gson.fromJson(jsonObject.toString(), WotUser.class);
        wotUser.nickname = wotUser.account_name;
        return wotUser;
    }

    public boolean hasIngame() {
        return hasIngame;
    }

    public String getRegionNickName(UserData_ userData) {
        return getRegionNickName(nickname, userData.region().get());
    }

    public boolean isOnline() {
        return lastOnline != null && System.currentTimeMillis() - lastOnline.getTime() < ONLINE_TIME;
    }

    public String getLastBattleTime(Resources resources) {
        long lastBattleMillis = last_battle_time * 1000;
        long now = Utils.truncToDay(new Date());
        long lastBattle = Utils.truncToDay(new Date(lastBattleMillis));
        if (now == lastBattle) {
            return resources.getString(R.string.todayBattleTime, timeFormat.format(new Date(lastBattleMillis)));
        }
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        if (Utils.truncToDay(calendar.getTime()) == lastBattle) {
            return resources.getString(R.string.yesterdayBattleTime, timeFormat.format(new Date(lastBattleMillis)));
        }
        return resources.getString(R.string.battleTime, dateFormat.format(new Date(lastBattleMillis)), timeFormat.format(new Date(lastBattleMillis)));
    }

    public JSONObject getPushObject(Context context) {
        JSONObject sendData = new JSONObject();
        JSONObject aps = new JSONObject();
        try {
            sendData.put("aps", aps);
            aps.put("alert", context.getString(R.string.worldOfNotification, nickname));
            aps.put("sound", "");
            sendData.put("action", "com.appgranula.ingame.INGAME_MESSAGE");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return sendData;
    }

    public String getPushMessage(Context context, String game) {
        return context.getString(R.string.worldOfNotification, nickname, context.getString(WargamingAPI.WOT.equals(game) ? R.string.worldOfTanks : R.string.worldOfWarPlanes));
    }

    public boolean isGroup() {
        return false;
    }

    public static class Comparator implements java.util.Comparator<WotUser> {

        @Override
        public int compare(WotUser wotUser, WotUser wotUser2) {
            if (wotUser.isGroup() && !wotUser2.isGroup())
                return -1;
            if (!wotUser.isGroup() && wotUser2.isGroup())
                return 1;
            if (wotUser.hasIngame() && !wotUser2.hasIngame())
                return -1;
            if (!wotUser.hasIngame() && wotUser2.hasIngame())
                return 1;
            int strCompare = wotUser.nickname.compareToIgnoreCase(wotUser2.nickname);
            if (strCompare > 0)
                return 1;
            if (strCompare < 0)
                return -1;
            return 0;
        }
    }


}

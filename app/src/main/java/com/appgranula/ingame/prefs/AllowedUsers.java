package com.appgranula.ingame.prefs;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by zaq on 29.09.2014.
 */
@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface AllowedUsers {
    public String clan();
    public String wotFriends();
    public String wowpFriends();
}

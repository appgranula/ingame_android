package com.appgranula.ingame.wot;

import java.io.Serializable;

/**
 * Created by Belozerov on 12.08.2014.
 */
public class WotUserPrivate implements Serializable{
    public long gold;
    public long free_xp;
    public long credits;
    public boolean is_bound_to_phone;
    public boolean is_premium;
    public long premium_expires_at;
    public Long[] friends;
}

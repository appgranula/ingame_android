package com.appgranula.ingame.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.appgranula.ingame.R;
import com.appgranula.ingame.parse.ParseAPI;
import com.appgranula.ingame.prefs.AllowedUsers_;
import com.appgranula.ingame.prefs.UserData_;
import com.appgranula.ingame.prefs.WatchingPref_;
import com.appgranula.ingame.utils.Utils;
import com.appgranula.ingame.widget.SwipeRefreshLayoutListViewFix;
import com.appgranula.ingame.widget.adapter.FriendsAdapter;
import com.appgranula.ingame.wot.WargamingAPI;
import com.appgranula.ingame.wot.WotClan;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import ch.lambdaj.Lambda;
import ch.lambdaj.function.matcher.Predicate;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Belozerov on 28.08.2014.
 */
@EFragment(R.layout.fragment_friends_list)
public class FriendsListFragment extends MainActivityFragment implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    public static final String TAG = "FriendsListFragment";
    private static final int FRIENDS_PER_REQUEST_LIMIT = 100;
    @FragmentArg
    public String game;
    @FragmentArg
    public boolean isClan;
    @Pref
    protected WatchingPref_ watchingPref;
    @ViewById
    StickyListHeadersListView friendsList;
    @ViewById
    SwipeRefreshLayoutListViewFix pullToRefresh;
    @ViewById
    TextView noFriendsText;
    @Bean
    ParseAPI parseAPI;
    @Bean
    WargamingAPI wargamingAPI;
    @Pref
    UserData_ userData;
    SearchView searchView;
    List<WotGroup> groups = new ArrayList<WotGroup>();
    @Pref
    AllowedUsers_ allowedUsers;
    private FriendsAdapter friendsAdapter;
    private List<WotUser> wotFriends = new ArrayList<WotUser>();
    private List<WotUser> tempWotFriends = new ArrayList<WotUser>();
    private int friendsOffset = 0;
    private int loaderCounter = 0;
    private boolean isError = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        activityCallback.setCurrentGroup(null);
        if (wotFriends.isEmpty()) {
            if (activityCallback.getCurrentUser(game) != null) {
                requestFriendsInfo(activityCallback.getCurrentUser(game).privateInfo.friends);
            }
        }
        if (isLoaded())
            notifyDataSetChanged();
    }

    @Override
    public String getFragmentSubTitle() {
        return WargamingAPI.WOT.equals(game) ? getString(R.string.worldOfTanksGroup) : getString(R.string.worldOfWarPlanesGroup);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(noFriendsText.getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuGroup:
                showAddGroupFragment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAddGroupFragment() {
        activityCallback.replaceFragment(AddGroupMembersFragment_.builder().game(game).isClan(isClan).build());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.searchHint));
        searchView.setOnQueryTextListener(this);
        Utils.styleSearchView(searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void clearSearch() {
        searchView.setQuery("", false);
    }

    @AfterViews
    void afterViews() {
        initList();
        initPullToRefresh();
    }

    public void sendFinished(final WotUser wotUser) {
        wotUser.isSending = false;
        wotUser.isSent = true;
        friendsList.postDelayed(new Runnable() {
            @Override
            public void run() {
                wotUser.isSent = false;
                updateAdapter();
            }
        }, 1000);
        updateAdapter();
    }

    @Override
    @UiThread
    public void notifyDataSetChanged() {
        if (getActivity() == null)
            return;
        List<WotUser> friends = activityCallback.getFriends();
        if (friends == null || friends.size() == 0) {
            showNoFriendsText();
            return;
        }
        sortList();
        if (!TextUtils.isEmpty(searchView.getQuery())) {
            filterFriendsList(searchView.getQuery().toString());
        } else {
            friendsAdapter.setUserList(friends);
            friendsAdapter.notifyDataSetChanged();
            hideCantLoadProblemText();
        }
        pullToRefresh.setRefreshing(false);
    }

    @Background
    public void sendIngame(final WotUser wotUser) {
        if (wotUser.isSending || wotUser.isSent)
            return;
        wotUser.isSending = true;
        updateAdapter();
        boolean sendResult = parseAPI.sendIngame(getActivity(), wotUser, activityCallback.getCurrentUser(game), game);
        if (sendResult) {
            sendFinished(wotUser);
        } else {
            showSendIngameError();
            wotUser.isSending = false;
            wotUser.isSent = false;
            updateAdapter();
        }
    }

    @UiThread
    protected void showSendIngameError() {
        if (getActivity() == null)
            return;
        Toast.makeText(getActivity(), R.string.ingameDidntSend, Toast.LENGTH_LONG).show();
    }

    @Override
    @UiThread
    public void parseError(Object e) {
        if (activityCallback == null)
            return;
        wotFriends.clear();
        activityCallback.getFriends().clear();
        isError = true;
        activityCallback.hideLoading();
        if (e == null || (e instanceof JSONObject) || (e instanceof ParseException)) {
            showCantLoadProblemText();
            if (e instanceof ParseException && ((ParseException) e).getCode() != ParseException.CONNECTION_FAILED) {
                Toast.makeText(getActivity(), ((Exception) e).getMessage(), Toast.LENGTH_LONG).show();
            }
            return;
        }
        if (e instanceof Exception) {
            Toast.makeText(getActivity(), ((Exception) e).getMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public boolean isAddToBackStack() {
        return false;
    }

    @UiThread
    protected void updateAdapter() {
        if (friendsAdapter != null)
            friendsAdapter.notifyDataSetChanged();
    }

    private void showNoFriendsText() {
        if (isError) {
            showCantLoadProblemText();
            return;
        }
        noFriendsText.setVisibility(View.VISIBLE);
        if (searchView != null && searchView.getQuery() != null) {
            if (TextUtils.isEmpty(searchView.getQuery().toString())) {
                noFriendsText.setText(getString(R.string.noFriendsText, (WargamingAPI.WOT.equals(game) ? getString(R.string.worldOfTanks) : getString(R.string.worldOfWarPlanes))));
            } else {
                noFriendsText.setText(getString(R.string.noFriendsFound, searchView.getQuery().toString()));
            }
        }
        friendsList.setVisibility(View.GONE);
        pullToRefresh.setRefreshing(false);
    }

    private void showCantLoadProblemText() {
        noFriendsText.setVisibility(View.VISIBLE);
        noFriendsText.setText(R.string.cantLoadText);
        friendsList.setVisibility(View.GONE);
        pullToRefresh.setRefreshing(false);
    }

    private void hideCantLoadProblemText() {
        noFriendsText.setVisibility(View.GONE);
        friendsList.setVisibility(View.VISIBLE);
    }

    private void initPullToRefresh() {
        pullToRefresh.setEnabled(false);
        pullToRefresh.setOnRefreshListener(this);
        pullToRefresh.setColorSchemeResources(R.color.pullColorFirst, R.color.pullColorSecond, R.color.pullColorThird, R.color.pullColorFourth);
        pullToRefresh.setListView(friendsList);
    }

    private void initList() {
        if (friendsAdapter == null) {
            friendsAdapter = new FriendsAdapter(getActivity());
        } else {
            friendsAdapter.setUserList(activityCallback.getFriends());
        }
        friendsAdapter.setOnCustomButtonClickListener(new FriendsAdapter.OnCustomButtonClickListener() {
            @Override
            public void onClick(WotUser wotUser) {
                if (wotUser instanceof WotGroup) {
                    openGroupInfo((WotGroup) wotUser);
                } else {
                    showStartWatchingDialog(wotUser);
                }
            }
        });
        friendsList.setAreHeadersSticky(false);
        friendsList.setAdapter(friendsAdapter);
        friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                WotUser user = friendsAdapter.getItem(i);
                if (user.hasIngame()) {
                    sendIngame(user);
                } else {
                    sendInvite(user);
                }
            }
        });
    }

    private void showStartWatchingDialog(final WotUser user) {
        if (!watchingPref.showAgain().get()) {
            startWatching(user);
            return;
        }
        if (user.isWatching) {
            startWatching(user);
            return;
        }
        View dialogView = View.inflate(getActivity(), R.layout.dialog_watching, null);
        final CheckBox dialogCheckbox = (CheckBox) dialogView.findViewById(R.id.dialogWatchingCheckbox);
        TextView dialogWatchingText = (TextView) dialogView.findViewById(R.id.dialogWatchingText);
        dialogWatchingText.setText(getString(R.string.startWatchingDialogText, user.nickname));
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog dialog = builder
                .setTitle(R.string.startWatchingItem)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        watchingPref.showAgain().put(!dialogCheckbox.isChecked());
                        startWatching(user);
                    }
                })
                .setView(dialogView)
                .create();
        dialog.show();
        Utils.styleAlertDialog(dialog);
    }

    private void startWatching(WotUser user) {
        if (user.isWatching) {
            user.isWatching = false;
            notifyDataSetChanged();
            parseAPI.stopWatching(user);
        } else {
            user.isWatching = true;
            notifyDataSetChanged();
            parseAPI.startWatching(user);
        }
    }

    private void openGroupInfo(WotGroup wotGroup) {
        EditGroupFragment editGroupFragment = EditGroupFragment_.builder()
                .isEdit(true)
                .isClan(isClan)
                .game(game)
                .build();
        activityCallback.setCurrentGroup(wotGroup);
        activityCallback.replaceFragment(editGroupFragment);
    }

    private void sendInvite(WotUser user) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, user.nickname, getString(WargamingAPI.WOT.equals(game) ? R.string.worldOfTanks : R.string.worldOfWarPlanes)));
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share)));
    }

    @Override
    public void onRefresh() {
        pullToRefresh.setRefreshing(true);
    }

    @Override
    public Spanned getFragmentTitle() {
        String title = getString(R.string.app_name);
        if (activityCallback != null && isClan && activityCallback.getClan(game) != null) {
            return activityCallback.getClan(game).getBeautifulName(getActivity());
        }

        if (activityCallback != null && activityCallback.getCurrentUser(game) != null)
            title = getString(R.string.friends);
        return Html.fromHtml(title);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        filterFriendsList(s);
        return true;
    }

    public void requestFriendsInfo(final Long[] friends) {
        isError = false;
        if(activityCallback == null){
            return;
        }
        loaderCounter++;
        activityCallback.showLoading();
        if (isClan) {
            WotClan clan = activityCallback.getClan(game);
            friendsOffset = 0;
            wotFriends.clear();
            activityCallback.getFriends().clear();
            wotFriends.addAll(clan.clanMembers);
            tempWotFriends.clear();
            loaderCounter--;
            requestIngameFriends();
            return;
        }
        if (friends.length == 0) {
            loaderCounter--;
            friendsOffset = 0;
            wotFriends.clear();
            activityCallback.getFriends().clear();
            notifyDataSetChanged();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkAllLoad();
                }
            }, 700);
            return;
        }
        final Long[] limitedFriends;
        if (friends.length > FRIENDS_PER_REQUEST_LIMIT) {
            limitedFriends = Arrays.copyOfRange(friends, friendsOffset, friendsOffset + FRIENDS_PER_REQUEST_LIMIT);
            friendsOffset += FRIENDS_PER_REQUEST_LIMIT;
        } else {
            limitedFriends = friends;
        }
        String ids = TextUtils.join(",", limitedFriends);
        wargamingAPI.requestUserInfo(ids, game, new WargamingAPI.APICallback() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    for (Long id : limitedFriends) {
                        if (!result.isNull(Long.toString(id))) {
                            tempWotFriends.add(WotUser.get(result.getJSONObject(Long.toString(id))));
                        }
                    }
                    if (friends.length > FRIENDS_PER_REQUEST_LIMIT && friendsOffset < friends.length) {
                        requestFriendsInfo(friends);
                        return;
                    }
                    friendsOffset = 0;
                    if (activityCallback != null && activityCallback.getFriends() != null) {
                        activityCallback.getFriends().clear();
                        if (activityCallback.getGroups() != null)
                            activityCallback.getGroups().clear();
                    }
                    wotFriends.clear();
                    wotFriends.addAll(tempWotFriends);
                    tempWotFriends.clear();
                    loaderCounter--;
                    requestIngameFriends();
                } catch (JSONException e) {
                    loaderCounter--;
                    e.printStackTrace();
                    parseError(null);
                }
            }

            @Override
            public void onError(JSONObject error) {
                parseError(error);
                loaderCounter--;
            }
        });
    }

    private void saveAllowedFriends() {
        parseAPI.saveAllowedFriends(wotFriends, game, isClan);
    }

    @Background
    public void requestIngameFriends() {
        loaderCounter++;
        List<String> friendsNames = new ArrayList<String>();
        for (WotUser friend : wotFriends) {
            friendsNames.add(friend.getRegionNickName(userData));
        }
        ParseQuery parseQuery = ParseUser.getQuery();
        parseQuery.whereContainedIn("wotNickname", friendsNames);
        parseQuery.whereEqualTo("active", true);
        parseQuery.setLimit(1000);
        try {
            ArrayList<ParseObject> answer = (ArrayList<ParseObject>) parseQuery.find();
            loaderCounter--;
            for (ParseObject parseUser : answer) {
                for (WotUser wotUser : wotFriends) {
                    if (wotUser.getRegionNickName(userData).equals(parseUser.get("wotNickname"))) {
                        wotUser.hasIngame = true;
                        wotUser.lastOnline = parseUser.getDate(game + "Online");
                        break;
                    }
                }
            }
            requestGroups();
        } catch (ParseException e) {
            loaderCounter--;
            parseError(e);
            e.printStackTrace();
        }
    }

    private void checkAllLoad() {
        if (loaderCounter == 0 && !isError) {
            if (activityCallback == null)
                return;
            sortList();
            activityCallback.setCurrentFriends(wotFriends);
            notifyDataSetChanged();
            activityCallback.hideLoading();
            saveAllowedFriends();
        }
    }

    private void sortList() {
        Collections.sort(wotFriends, new WotUser.Comparator());
    }

    @Background
    protected void requestGroups() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if(activityCallback == null){
            return;
        }
        if (activityCallback.getGroups() != null)
            activityCallback.getGroups().clear();
        loaderCounter++;
        try {
            currentUser.fetch();
            ArrayList<String> groups = (ArrayList<String>) currentUser.get(WotGroup.getGroupField(game, isClan));
            ArrayList<String> watchList = (ArrayList<String>) currentUser.get("watchList");
            if (groups == null) {
                loaderCounter--;
                checkAllLoad();
                return;
            }
            HashMap<String, WotGroup> groupsMap = new HashMap<String, WotGroup>();
            String groupName;
            WotUser wotUser = null;
            WotGroup group;
            for (String groupEntity : groups) {
                groupName = groupEntity.substring(groupEntity.indexOf('.') + 1, groupEntity.length());
                final String userName = groupEntity.substring(0, groupEntity.indexOf('.'));
                group = groupsMap.get(groupName);
                if (group == null) {
                    group = new WotGroup();
                    group.nickname = groupName;
                    groupsMap.put(groupName, group);
                }
                wotUser = Lambda.selectFirst(wotFriends, new Predicate<WotUser>() {
                    @Override
                    public boolean apply(WotUser item) {
                        return item.getRegionNickName(userData).equals(userName);
                    }
                });
                if (wotUser != null) {
                    group.members.add(wotUser);
                }
            }
            if (watchList != null) {
                for (WotUser watching : wotFriends) {
                    watching.isWatching = watchList.contains(watching.getRegionNickName(userData));
                }
            }
            this.groups.clear();
            this.groups.addAll(groupsMap.values());
            this.wotFriends.addAll(this.groups);
            loaderCounter--;
            checkAllLoad();
        } catch (ParseException e) {
            loaderCounter--;
            parseError(e);
            e.printStackTrace();
        }
    }

    private void filterFriendsList(final String s) {
        if (isLoaded() && friendsAdapter != null) {
            friendsAdapter.setUserList(Lambda.filter(new Predicate<WotUser>() {
                @Override
                public boolean apply(WotUser o) {
                    return o.nickname.toLowerCase().contains(s.toLowerCase());
                }
            }, activityCallback.getFriends()));
            friendsAdapter.notifyDataSetChanged();
            if (friendsAdapter.getCount() == 0) {
                showNoFriendsText();
            } else {
                hideCantLoadProblemText();
            }
        }
    }

    private boolean isLoaded() {
        return loaderCounter == 0;
    }
}

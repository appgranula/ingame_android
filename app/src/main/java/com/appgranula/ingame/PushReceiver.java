package com.appgranula.ingame;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.appgranula.ingame.prefs.AllowedUsers_;
import com.appgranula.ingame.utils.Utils;
import com.appgranula.ingame.wot.WargamingAPI;
import com.parse.ParseUser;

import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Belozerov on 12.08.2014.
 */
@EReceiver
public class PushReceiver extends BroadcastReceiver {
    private static final String EXTRA_FROM_PUSH = "extra_from_push";
    @SystemService
    NotificationManager notificationManager;
    @Pref
    AllowedUsers_ allowedUsers;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ParseUser.getCurrentUser() == null)
            return;
        try {
            JSONObject jsonData = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            int requestID = (int) System.currentTimeMillis();
            String nickname = jsonData.getJSONObject("sender").getString("nickname");
            String game = jsonData.getJSONObject("sender").getString("game");
            nickname = nickname.substring(Utils.nthOccurrence(nickname, '_', 1) + 1);
            String message = context.getString(R.string.pushMessage, nickname, context.getString(WargamingAPI.WOT.equals(game) ? R.string.worldOfTanks : R.string.worldOfWarPlanes));
            String title = context.getString(R.string.app_name);
            Intent ingameIntent = new Intent(context, MainActivity_.class);
            ingameIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            ingameIntent.putExtra(EXTRA_FROM_PUSH, true);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestID, ingameIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setTicker(message)
                    .setAutoCancel(true)
                    .setLights(0xffffffff, 1000, 5000)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            notificationManager.notify(message.hashCode(), builder.build());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:/Program Files (x86)/Android/android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class org.** { *; }
-keep class javax.** { *; }
-keep class com.vk.** { *; }
-keep class io.card.** { *; }
-keep class twitter4j.** { *; }
-keep class com.parse.** { *; }
-keep class com.paypal.** { *; }
-keep class com.facebook.** { *; }
-keep class com.squareup.** { *; }
-keep class android.support.** { *; }
-keep class net.sourceforge.** { *; }
-keep class com.android.datetimepicker.** { *; }
-keep class com.appgranula.ingame.parse.** { *; }
-keep class com.appgranula.ingame.wot.WotClan { *; }
-keep class com.appgranula.ingame.wot.WotEmblem { *; }
-keep class com.appgranula.ingame.wot.WotGroup { *; }
-keep class com.appgranula.ingame.wot.WotUser { *; }
-keep class com.appgranula.ingame.wot.WotUserPrivate { *; }
-keep class com.squareup {*;}
-keep class com.daimajia {*;}
-keep class net {*;}

-keepattributes *Annotation*,SourceFile,LineNumberTable
-keepattributes Signature

-dontwarn org.**
-dontwarn twitter4j.**
-dontwarn com.paypal.**
-dontwarn com.facebook.**
-dontwarn com.parse.**
-dontwarn android.support.**
-dontwarn com.squareup.**
-dontwarn net.sourceforge.**
-dontwarn com.squareup.okhttp.**
-dontwarn com.squareup.**
-dontwarn com.daimajia.**
-dontwarn net.**
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
package com.appgranula.ingame;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Belozerov on 25.08.2014.
 */
@EActivity(R.layout.fragment_about)
public class AboutActivity extends ActionBarActivity {
    @ViewById
    TextView rateContest;
    @ViewById
    TextView copyright;
    @ViewById
    TextView wgdcContest;
    @ViewById
    TextView appVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @AfterViews
    void init() {
        rateContest.setText(Html.fromHtml(getString(R.string.aboutRateUs)));
        copyright.setText(Html.fromHtml(getString(R.string.copyright)));
        wgdcContest.setText(Html.fromHtml(getString(R.string.wgdcContest)));
        try {
            appVersion.setText(getString(R.string.appVersion, getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Click
    void appgranula(){
        openUrl(getString(R.string.appgranulaUrl));
    }

    @Click
    void copyright() {
        openUrl(getString(R.string.copyrightUrl));
    }

    @Click
    void wgdcContest() {
        openUrl(getString(R.string.wgdcContestUrl));
    }

    @Click
    void rateContest() {
        openUrl(getString(R.string.rateContestUrl));
    }

    private void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.appgranula.ingame.widget.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.wot.WotUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Belozerov on 28.08.2014.
 */
public class GroupMemberAdapter extends BaseAdapter {
    List<WotUser> users;
    ArrayList<String> selected = new ArrayList<String>();
    LayoutInflater inflater;

    public GroupMemberAdapter(Context context, List<WotUser> users) {
        this.users = users;
        this.inflater = LayoutInflater.from(context);
    }

    public void setUsers(List<WotUser> users){
        this.users = users;
    }


    public void setSelected(ArrayList<String> selected) {
        this.selected = selected;
    }

    public ArrayList<String> getSelected() {
        return selected;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public WotUser getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.member_item, viewGroup, false);
            holder = new ViewHolder();
            holder.nickname = (TextView) view.findViewById(R.id.friendNickName);
            holder.checkBox = (CheckBox) view.findViewById(R.id.friendCheckbox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        WotUser user = getItem(i);

        holder.nickname.setText(user.nickname);
        holder.checkBox.setChecked(selected.contains(user.nickname));
        return view;
    }

    static class ViewHolder {
        TextView nickname;
        CheckBox checkBox;
    }
}

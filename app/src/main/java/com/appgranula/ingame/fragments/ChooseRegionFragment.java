package com.appgranula.ingame.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.appgranula.ingame.R;
import com.appgranula.ingame.StartActivity;
import com.appgranula.ingame.prefs.UserData_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Belozerov on 19.08.2014.
 */
@EFragment(R.layout.fragment_region)
public class ChooseRegionFragment extends Fragment {
    public static final String TAG = "ChooseRegionFragment";
    @ViewById
    ListView regionList;
    @Pref
    UserData_ userData;


    @AfterViews
    void init() {
        ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        String[] regions = new String[]{
                getString(R.string.russia),
                getString(R.string.europa),
                getString(R.string.northAmerica),
                getString(R.string.asia),
                getString(R.string.korea)
        };
        Integer[] flags = new Integer[]{
                R.drawable.login_flag_ru,
                R.drawable.login_flag_eu,
                R.drawable.login_flag_us,
                R.drawable.login_flag_sea,
                R.drawable.login_flag_kr
        };
        final String[] langs = {
                "ru",
                "eu",
                "na",
                "asia",
                "kr"
        };
        ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(
                regions.length);
        Map<String, Object> m;
        for (int i = 0; i < regions.length; i++) {
            m = new HashMap<String, Object>();
            m.put("region", regions[i]);
            m.put("flag", flags[i]);
            data.add(m);
        }

        String[] from = {"region", "flag"};
        int[] to = {R.id.regionName, R.id.regionFlag};
        SimpleAdapter sAdapter = new SimpleAdapter(getActivity(), data, R.layout.region_item,
                from, to);
        regionList.setAdapter(sAdapter);
        regionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                userData.region().put(langs[i]);
                ((StartActivity) getActivity()).showWebView();
            }
        });
    }
}

package com.appgranula.ingame.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import com.appgranula.ingame.R;

/**
 * Created by belozerov on 19.03.14.
 */

public final class ProgressDialogFragment extends DialogFragment {

    private static final String ARG_MESSAGE = "message";
    private static final String TAG = "progress_dialog_fragment";

    public static void show(FragmentActivity activity) {
        if(activity == null)
            return;
        DialogFragment progressDialog =
                (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag(TAG);
        if (progressDialog == null) {
            progressDialog = create();
            progressDialog.setCancelable(false);
            progressDialog.show(activity.getSupportFragmentManager(), TAG);
        }
    }

    public static void showCancelable(FragmentActivity activity) {
        DialogFragment progressDialog =
                (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag(TAG);
        if (progressDialog == null) {
            progressDialog = create();
            progressDialog.show(activity.getSupportFragmentManager(), TAG);
        }
    }

    public static void hide(FragmentActivity activity) {
        if(activity == null)
            return;
        DialogFragment progressDialog =
                (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag(TAG);
        if (progressDialog != null) {
            progressDialog.dismissAllowingStateLoss();
        }
    }

    public static ProgressDialogFragment create(int message) {
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MESSAGE, message);
        progressDialogFragment.setArguments(args);
        return progressDialogFragment;
    }

    public static ProgressDialogFragment create() {
        return create(R.string.progress_message);
    }

    public static boolean isShowing(FragmentActivity activity) {
        DialogFragment progressDialog =
                (DialogFragment) activity.getSupportFragmentManager().findFragmentByTag(TAG);
        return progressDialog != null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(getArguments().getInt(ARG_MESSAGE)));
        return progressDialog;
    }
}


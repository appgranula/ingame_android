package com.appgranula.ingame.wot;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;

import com.appgranula.ingame.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Belozerov on 12.09.2014.
 */
public class WotClan implements Serializable {
    public String color;
    public String name;
    public WotEmblem emblems;
    public String abbreviation;
    public JsonObject members;

    public List<WotUser> clanMembers = new ArrayList<WotUser>();

    public static WotClan get(JSONObject jsonObject, WotUser currentUser) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        WotClan wotClan = gson.fromJson(jsonObject.toString(), WotClan.class);
        WotUser member;
        for(Map.Entry<String, JsonElement> entry : wotClan.members.entrySet()){
            member = WotUser.get(entry.getValue());
            if(currentUser.nickname.equals(member.nickname)){
                continue;
            }
            wotClan.clanMembers.add(member);
        }
        return wotClan;
    }

    public Spanned getBeautifulName(Context context) {
        return Html.fromHtml(context.getString(R.string.clanTemplate, color, abbreviation, name));
    }
}

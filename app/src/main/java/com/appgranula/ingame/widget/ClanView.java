package com.appgranula.ingame.widget;

import android.content.Context;
import android.text.Html;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.widget.adapter.DrawerAdapter;
import com.appgranula.ingame.wot.WotClan;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Belozerov on 19.09.2014.
 */
@EViewGroup(R.layout.item_menu)
public class ClanView extends LinearLayout{
    @ViewById
    TextView menuItemText;
    @ViewById
    ImageView menuItemIcon;

    public ClanView(Context context) {
        super(context);
    }
    public void bind(DrawerAdapter.DrawerClanItem clan){
        Picasso.with(getContext()).load(clan.clan.emblems.large).into(menuItemIcon);
        menuItemText.setText(clan.clan.getBeautifulName(getContext()));
    }

}

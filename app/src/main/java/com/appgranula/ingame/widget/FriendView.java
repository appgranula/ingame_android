package com.appgranula.ingame.widget;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.widget.adapter.FriendsAdapter;
import com.appgranula.ingame.wot.WotUser;
import com.daimajia.swipe.SwipeLayout;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by zaq on 23.09.2014.
 */
@EViewGroup(R.layout.friend_item)
public class FriendView extends FrameLayout {
    @ViewById
    SwipeLayout swipeLayout;
    @ViewById
    TextView friendNickName;
    @ViewById
    View friendOnline;
    @ViewById
    ImageView friendWatching;
    @ViewById
    FrameLayout friendSending;
    @ViewById
    ProgressBar friendSendingProgress;
    @ViewById
    TextView friendSent;
    @ViewById
    TextView watchingText;

    public FriendView(Context context) {
        super(context);
    }

    public void bind(final WotUser user, final FriendsAdapter.OnCustomButtonClickListener onCustomButtonClickListener){
        friendNickName.setText(user.nickname);
        friendSending.setVisibility(user.isSending || user.isSent ? View.VISIBLE : View.GONE);
        friendSent.setVisibility(user.isSent ? View.VISIBLE : View.GONE);
        friendSendingProgress.setVisibility(user.isSending ? View.VISIBLE : View.GONE);
        friendOnline.setVisibility(user.isOnline() ? View.VISIBLE : View.GONE);
        friendWatching.setVisibility(user.isWatching ? View.VISIBLE : View.GONE);
        watchingText.setText(user.isWatching?R.string.stopWatchingItem :R.string.startWatchingItem);
        if(onCustomButtonClickListener != null) {

            swipeLayout.getBottomView().setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCustomButtonClickListener.onClick(user);
                    swipeLayout.close(true);
                }
            });
        } else {
            swipeLayout.setSwipeEnabled(false);
        }
    }
}

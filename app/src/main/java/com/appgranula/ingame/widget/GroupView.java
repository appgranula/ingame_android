package com.appgranula.ingame.widget;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appgranula.ingame.R;
import com.appgranula.ingame.widget.adapter.FriendsAdapter;
import com.appgranula.ingame.wot.WotGroup;
import com.appgranula.ingame.wot.WotUser;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by zaq on 23.09.2014.
 */
@EViewGroup(R.layout.group_item)
public class GroupView extends FrameLayout {
    @ViewById
    ImageView groupInfo;
    @ViewById
    TextView groupName;
    @ViewById
    TextView groupOnline;
    @ViewById
    FrameLayout friendSending;
    @ViewById
    ProgressBar friendSendingProgress;
    @ViewById
    TextView friendSent;

    public GroupView(Context context) {
        super(context);
    }

    public void bind(WotUser user, final FriendsAdapter.OnCustomButtonClickListener onCustomButtonClickListener) {
        final WotGroup wotGroup = (WotGroup) user;
        groupName.setText(wotGroup.nickname);
        groupOnline.setText(wotGroup.getOnlineCountString(getResources()));
        groupInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onCustomButtonClickListener != null)
                    onCustomButtonClickListener.onClick(wotGroup);
            }
        });
        friendSending.setVisibility(wotGroup.isSending || wotGroup.isSent ? View.VISIBLE : View.GONE);
        friendSent.setVisibility(wotGroup.isSent ? View.VISIBLE : View.GONE);
        friendSendingProgress.setVisibility(wotGroup.isSending ? View.VISIBLE : View.GONE);
    }
}

package com.appgranula.ingame.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.logging.FileHandler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;


/**
 * Created by beloz_000 on 09.07.2014.
 */
public class Log_ {
    private static final String FILE_NAME = "ingame_log.txt";
    private static String FILE_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + FILE_NAME;
    private static final int LIMIT = 1024 * 1024;
    private static final String TAG = "INGAME_LOGGER";
    private static ExecutorService executor = null;
    private static Logger fileLogger;

    public static void init(Context context) {
        File external = context.getExternalFilesDir(null);
        if (external != null)
            FILE_PATH = external.getPath() + File.separator + FILE_NAME;

        fileLogger = Logger.getLogger(TAG);
        try {
            FileHandler fileHandler = new FileHandler(FILE_PATH, LIMIT, 1, true);
            fileHandler.setLevel(java.util.logging.Level.ALL);
            fileHandler.setFormatter(new java.util.logging.Formatter() {
                @Override
                public String format(LogRecord logRecord) {
                    return logRecord.getMessage();
                }
            });
            fileLogger.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Thread.UncaughtExceptionHandler exceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                Log_.e(android.util.Log.getStackTraceString(throwable));
                exceptionHandler.uncaughtException(thread, throwable);
            }
        });
    }

    private static String buildMessage(Level level, String msg, String action) {
        StackTraceElement e = Thread.currentThread().getStackTrace()[4];
        return getTime() +
                "\t" + level.toString() +
                "\t" + e.getClassName() +
                "\t" + e.getMethodName() +
                "\t" + e.getLineNumber() +
                (action != null ? ("\t" + action) : "") +
                "\t" + msg +
                "\n";
    }

    public static void e(String message) {
        fileLogger.severe(buildMessage(Level.e, message, null));
    }

    public static void e(String action, String message) {
        fileLogger.severe(buildMessage(Level.e, message, action));
    }

    public static void i(String action, String message) {
        fileLogger.log(java.util.logging.Level.INFO, buildMessage(Level.i, message, action));
    }

    public static void i(String message) {
        fileLogger.log(java.util.logging.Level.INFO, buildMessage(Level.i, message, null));
    }

    public static void w(String message) {
        fileLogger.log(java.util.logging.Level.WARNING, buildMessage(Level.w, message, null));
    }

    public static void w(String action, String message) {
        fileLogger.log(java.util.logging.Level.WARNING, buildMessage(Level.w, message, action));
    }

    public static void d(String action, String message) {
        fileLogger.log(java.util.logging.Level.INFO, buildMessage(Level.d, message, action));
    }

    public static void d(String message) {
        fileLogger.log(java.util.logging.Level.INFO, buildMessage(Level.d, message, null));
    }

    public static void v(String action, String message) {
        fileLogger.log(java.util.logging.Level.INFO, buildMessage(Level.v, message, action));
    }

    public static void v(String message) {
        fileLogger.log(java.util.logging.Level.INFO, buildMessage(Level.v, message, null));
    }



    public static Uri getUri() {
        return Uri.fromFile(getLogFile());
    }


    private static String getTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return simpleDateFormat.format(new Date()) + "\t" + TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT);
    }

    private static File getLogFile() {
        File file = new File(FILE_PATH);
        boolean isExists = file.exists();
        if (isExists) {
            return file;
        } else {
            try {
                file.createNewFile();
                return file;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private enum Level {v, d, i, w, e}
}

package com.appgranula.ingame;

import android.app.Application;

import com.appgranula.ingame.utils.Log_;
import com.parse.Parse;
import com.parse.PushService;

/**
 * Created by Belozerov on 11.08.2014.
 */
public class IngameApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Log_.init(this);
        Parse.initialize(this, Constants.PARSE_APP_ID, Constants.PARSE_CLIENT_KEY);
        PushService.setDefaultPushCallback(this, MainActivity_.class);
    }
}
